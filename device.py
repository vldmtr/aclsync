from scrapli.driver.core import IOSXEDriver
from scrapli.exceptions import ScrapliException


natRouter1 = {
    "host": "172.16.10.237",
    "auth_username": "admin",
    "auth_password": "admin",
    "auth_secondary": "admin",
    "auth_strict_key": False,
    "timeout_socket": 5,  # timeout for establishing socket/initial connection
    "timeout_transport": 10,  # timeout for ssh|telnet transport
}


def configureRouter(device, commands):
    try:
        with IOSXEDriver(**device) as ssh:
            reply = ssh.send_commands(commands)
            return reply.result
        except ScrapliException as error:
            print(error, device["host"])
