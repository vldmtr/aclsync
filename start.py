import signal
import daemon
import lockfile
import sys

from acl_sync import runDaemon
from config import initConfig, reloadConfig


context = daemon.DaemonContext(
    detach_process=True,
    stdout=sys.stdout,
    stderr=sys.stderr,
    pidfile=lockfile.FileLock('/var/run/nat_sync.pid')
    )

context.signal_map = {
    signal.SIGTERM: 'terminate',
    signal.SIGHUP: reloadConfig
    }

if __name__ == "__main__":
    print('Run NAT ACL sync daemon')
    initConfig()
    with context:
        runDaemon()
