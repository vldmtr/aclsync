# Global variables submodule
from dns import inet

def loadHosts(filename):

    #TODO subnets
    d = {'synced': False, 'dstList': list(), 'domains': list(), 'cache': dict()}
    with open(filename, 'r') as f:
        for line in f:
            entry = line.rstrip().split('/')
            if inet.is_address(entry[0]) and len(entry) == 2:
                d['dstList'].append('/'.join(entry))
            elif inet.is_address(entry[0]) and len(entry) == 1:
                d['dstList'].append(entry[0] + '/32')
            else:
                d['domains'].append(entry[0])
    return d


def initConfig():
    global data
    global hosts
    hosts = '/tmp/hosts.txt'
    data = loadHosts(hosts)


def reloadConfig(signum, frame):
    hosts = frame.f_globals['config'].hosts
    print(f'Got HUP, reloading daemon config from {hosts}...')
    frame.f_globals['config'].data = loadHosts(hosts)
