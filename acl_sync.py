import time
import config

from dns import resolver,inet
from device import natRouter1, configureRouter

def resolveDomain(domain):
    answer = None
    try:
        answer = resolver.resolve(domain)
        for rdata in answer:
            print(f'Host {domain} has IP-address {rdata.address} with TTL {answer.ttl}')
    except:
        print(f'Unable to resolve {domain}')
    return answer

def updateConfig(dstList):
    print('Syncing ACL...')
    commands = ['conf t', 'no ip access-list extended NAT-golden', 'ip access-list extended NAT-golden']
    seq = 10
    for dst in dstList:
        entry = dst.split('/')
        if entry[1] == '32':
            commands.append(f'{seq} permit ip 10.0.0.0 0.0.0.255 host {entry[0]}')
        else:
            commands.append(f'{seq} permit ip 10.0.0.0 0.0.0.255 any {dst}')
        seq += 10
    configureRouter(natRouter1, commands)


def runDaemon():

    while True:

        data = config.data
        for d in data['domains']:
            dnsData = data['cache'].get(d)

            if not dnsData or dnsData.expiration <= time.time():
                dnsData = resolveDomain(d)
                data['cache'][d] = dnsData

            if dnsData:
                newHosts = [f'{host.address}/32' for host in dnsData if f'{host.address}/32' not in data['dstList']]
                if len(newHosts) > 0:
                    data['synced'] = False
                    data['dstList'] += newHosts

        if not data['synced']:
            try:
                updateConfig(data['dstList'])
                data['synced'] = True
            except:
                print('Sync failed!')
        time.sleep(3)
